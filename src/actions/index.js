import _ from 'lodash';

export const UPDATE_PATTERN = 'UPDATE_PATTERN';
export const REQUEST_USERS = 'REQUEST_USERS';
export const RECEIVE_USERS = 'RECEIVE_USERS';
export const SELECT_USER = 'SELECT_USER';

// Исключительно для эмуляции получения данных!
let data = [
    {"id": 1, "name": "Tom Hanks"},
    {"id": 2, "name": "Bruce Willis"},
    {"id": 3, "name": "Tom Cruise"},
    {"id": 4, "name": "Matt Damon"},
    {"id": 5, "name": "Michael Caine"},
    {"id": 6, "name": "Samuel L. Jackson"},
    {"id": 7, "name": "Tommy Lee Jones"},
    {"id": 8, "name": "Gary Oldman"},
    {"id": 9, "name": "Will Smith"},
    {"id": 10, "name": "Harrison Ford"},
    {"id": 11, "name": "Jack Nicholson"},
    {"id": 12, "name": "Robert De Niro"},
    {"id": 13, "name": "Al Pacino"},
    {"id": 14, "name": "Anthony Hopkins"},
    {"id": 15, "name": "Clint Eastwood"}
];

/**
 *
 * @param id - идентификатор корневого элемента (селекта)
 * @param pattern - шаблон поиска
 */
export const updatePattern = (id, pattern) => ({
    type: UPDATE_PATTERN,
    id,
    pattern
});

/**
 *
 * @param id - идентификатор корневого элемента (селекта)
 * @param idFetch - идентификатор запроса на получения данных (дает возможность проигнорировать неактуальный запрос)
 */
export const requestUsers = (id, idFetch) => ({
    type: REQUEST_USERS,
    id,
    idFetch
});

export const receiveUsers = (id, users, idFetch) => ({
    type: RECEIVE_USERS,
    id,
    users,
    idFetch
});

export const fetchUsers = (id) => dispatch => {
    
    // Цикл получения данных включает в себя 3 этапа: 1) requestUsers, 2) fetchUsers, 3) receiveUsers
    // Он может быть прераван, если инициирован новый запрос на получение данных.
    // Для того, чтобы иметь возможность его прервать мы сохраняем idFetch
    // (логика idFetch обрабатывается в reducers\index.js на RECEIVE_USERS)
    
    const idFetch = _.uniqueId();
    dispatch(requestUsers(id, idFetch));
    
    // Иммитация получения данных
    return new Promise(function(resolve, reject) {
        setTimeout(resolve, 500, data);
    })
    .then(users => dispatch(receiveUsers(id, users, idFetch)));
};

export const selectUser = (id, userKey) => ({
    type: SELECT_USER,
    id,
    userKey,
});
