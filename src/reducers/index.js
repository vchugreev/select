import { UPDATE_PATTERN, REQUEST_USERS, RECEIVE_USERS, SELECT_USER } from '../actions';

const USERS_LIMIT = 10;

const reducer = function(state = {}, action) {
    switch(action.type) {
        case UPDATE_PATTERN:
            return Object.assign({}, state, {
                [action.id]: {
                    pattern: action.pattern,
                    users: []
                }
            });
            
        case REQUEST_USERS:
            return Object.assign({}, state, {
                [action.id]: {
                    pattern: state[action.id].pattern,
                    users: state[action.id].users,
                    idFetch: action.idFetch
                }
            });
            
        case RECEIVE_USERS:
            
            // Если во время получения данных, был сгенерирован новый запрос, старый игнорируем, т.е. полученные
            // данные от предыдущего запроса никак не проявят себя (чтобы мелькания списка не было)
            if (action.idFetch !== state[action.id].idFetch) {
                return state;
            }
    
            const pattern = state[action.id].pattern;
            
            return Object.assign({}, state, {
                [action.id]: {
                    pattern: pattern,
                    users: filterAndLimiter(action.users, pattern, USERS_LIMIT)
                }
            });
    
        case SELECT_USER:
            const user = getUser(state, action.id, action.userKey);
            return Object.assign({}, state, {
                [action.id]: {
                    pattern: user.name,
                    users: []               // При выборе конкретного пользователя список пользователей сбрасываем
                }
            });
            
        default:
            return state;
    }
};

function getUser(state, id, userKey) {
    return state[id].users.find((item) => item.id === Number(userKey));
}

function filterAndLimiter(data, pattern, limit = 0) {
    
    let result = data.filter((item) => item.name.toLowerCase().includes(pattern.toLowerCase()));
    
    // Здесь не самый лучший вариант лимитирования, в рабочем проекте можно возвращать всех пользователей,
    // а лимитировать их уже на самом контроле (при отоображении списка пользователей), показывая, что есть еще...
    // Если пользователей ОЧЕНЬ много, то лимитировать можно и здесь и там, например, здесь ограничиваем
    // в пределах десяток-сотен людей, а там уже показываем в пределах 5-20 человек
    
    if (limit > 0) {
        result = result.slice(0, limit);
    }
    
    return result;
}

export default reducer;
