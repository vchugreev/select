import React, { PropTypes } from 'react';

class Input extends React.Component {

    constructor(props) {
        super(props);
        this.state = { value: this.props.pattern };
        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(event) {
        this.setState({ value: event.target.value }, this.applyChange);
    }
    
    applyChange() {
        this.props.updatePattern(this.state.value);
        if (this.state.value) {         // Получать пользователей будем только для непустого шаблона
            this.props.fetchUsers();
        }
    }
    
    componentWillReceiveProps(nextProps) {
        this.setState({ value: nextProps.pattern });
    }
    
    render() {
        return (
            <div className="input">
                <input type="text" value={this.state.value} onChange={this.handleChange} />
                {
                    this.props.isFetch &&
                        <img src={'/img/loader.gif'} role="presentation" />
                }
            </div>
        );
    }
}

Input.propTypes = {
    pattern: PropTypes.string.isRequired,
    isFetch: PropTypes.bool.isRequired,
    updatePattern: PropTypes.func.isRequired,
    fetchUsers: PropTypes.func.isRequired
};

export default Input;