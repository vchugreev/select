import React from 'react';

export default class User extends React.Component {
    
    render() {
        return (
            <div data-key={this.props.userId} className="user-list-item">
                {this.props.userName}
            </div>
        );
    }
}

User.propTypes = {
    userId: React.PropTypes.number.isRequired,
    userName: React.PropTypes.string.isRequired
};
