import React from 'react';

export default class UserList extends React.Component {
    
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick(event) {
        this.props.selectUser(event.target.dataset.key);
    }
    
    render() {
        return (
            <div className="user-list" onClick={this.handleClick}>
                {this.props.children}
            </div>
        );
    }
}

UserList.propTypes = {
    selectUser: React.PropTypes.func.isRequired
};



