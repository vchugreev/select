import React from 'react';
import { connect } from 'react-redux';
import { updatePattern, fetchUsers, selectUser } from '../actions';
import Input from '../components/input';
import UserList from '../components/userList';
import User from '../components/user';

class Select extends React.Component {

    render() {
        return (
            <div className="select">
                <Input pattern={this.props.pattern}
                       updatePattern={this.props.updatePattern}
                       fetchUsers={this.props.fetchUsers}
                       isFetch={this.props.isFetch} />
                
                <UserList selectUser={this.props.selectUser}>
                    {this.props.users.map(user => {
                        return (
                            <User key={user.id} userId={user.id} userName={user.name} />
                        );
                    })}
                </UserList>
            </div>
        );
    }
}

Select.propTypes = {
    id: React.PropTypes.string.isRequired
};

const mapStateToProps = function(store, ownProps) {
    const state = store[ownProps.id];
    return {
        isFetch: state ? Boolean(state.idFetch) : false,
        pattern: state ? state.pattern : '',
        users: state ? state.users : []
    };
};

const mapDispatchToProps = function(dispatch, ownProps) {
    return {
        updatePattern: (pattern) => {
            dispatch(updatePattern(ownProps.id, pattern));
        },
        fetchUsers: () => {
            dispatch(fetchUsers(ownProps.id));
        },
        selectUser: (userKey) => {
            dispatch(selectUser(ownProps.id, userKey));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Select);